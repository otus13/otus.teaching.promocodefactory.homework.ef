﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository
                .GetAll()
                .ToListAsync();

            var customerShortResponse = customers
                .Select(c => new CustomerShortResponse(c))
                .ToList();

            return customerShortResponse;
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository
                .GetAll()
                .Include(c => c.PromoCodes)
                .Include(c => c.CustomerPreferences)
                    .ThenInclude(c => c.Preference)
                .FirstOrDefaultAsync(c => c.Id == id);

            if (customer == null)
                return NotFound();

            var customerResponse = new CustomerResponse(customer);

            return customerResponse;
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = new List<CustomerPreference>()
            };

            _customerRepository.Add(customer);

            await _customerRepository.SaveChangesAsync();

            request.PreferenceIds.ForEach(p =>
                customer.CustomerPreferences.Add(
                    new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        PreferenceId = p
                    })
            );

            await _customerRepository.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Редактировать данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository
                .GetAll()
                .Include(c => c.PromoCodes)
                .Include(c => c.CustomerPreferences)
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();

            if (customer == null)
                return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences.Clear();

            request.PreferenceIds.ForEach(p =>
                customer.CustomerPreferences.Add(
                    new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        PreferenceId = p
                    })
            );

            await _customerRepository.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository
                .GetAll()
                .Include(c => c.PromoCodes)
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync();

            if (customer == null)
                return Ok();

            _promoCodeRepository.RemoveRange(customer.PromoCodes);
            _customerRepository.Remove(customer);

            await _customerRepository.SaveChangesAsync();

            return Ok();
        }
    }
}