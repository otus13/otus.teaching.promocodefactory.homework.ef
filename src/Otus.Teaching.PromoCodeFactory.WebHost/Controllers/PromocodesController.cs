﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAll().ToListAsync();

            var response = promoCodes.Select(p => new PromoCodeShortResponse(p)).ToList();

            return response;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository
                .GetAll()
                .Where(p => p.Name == request.Preference)
                .FirstOrDefaultAsync();

            if (preference == null)
                return NotFound();

            var promoCode = new PromoCode()
            {
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                Preference = preference
            };

            _promoCodeRepository.Add(promoCode);

            var customers = await _customerRepository
                .GetAll()
                .Include(c => c.CustomerPreferences)
                .Include(c => c.PromoCodes)
                .Where(c => c.CustomerPreferences
                    .Any(cp => cp.PreferenceId == preference.Id))
                .ToListAsync();

            //Only one customer gets updated because Customer and Promocode relations is one-to-many
            customers.ForEach(c => c.PromoCodes.Add(promoCode));

            await _promoCodeRepository.SaveChangesAsync();

            return Ok();
        }
    }
}