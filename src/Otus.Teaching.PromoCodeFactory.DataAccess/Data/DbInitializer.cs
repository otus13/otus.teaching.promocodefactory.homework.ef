﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class DbInitializer
    {
        public static void InitializeDb(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<EfDataContext>();

            if (context != null)
            {
                if (context.Database.GetPendingMigrations().Any())
                    context.Database.Migrate();
            }
        }
    }
}
