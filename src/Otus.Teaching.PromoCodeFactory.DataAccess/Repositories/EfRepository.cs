﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly DbContext _context;

        private DbSet<T> Table => _context.Set<T>();

        public EfRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> GetAll()
        {
            return Table;
        }

        public T Find(Guid id)
        {
            return Table.Find(id);
        }

        public void Add(T entity)
        {
            Table.Add(entity);
        }

        public void Remove(T entity)
        {
            Table.Remove(entity);
        }
        public void RemoveRange(IEnumerable<T> entities)
        {
            Table.RemoveRange(entities);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}